libconfig-model-systemd-perl (0.247.1-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.247.1.
    * update model from systemd 247 documentation

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Dec 2020 15:52:58 +0100

libconfig-model-systemd-perl (0.246.1-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

  [ gregor herrmann ]
  * Import upstream version 0.246.1.
    * update parameters from systemd 246 source
    * model doc now provides URL links from systemd doc
  * Update debian/upstream/metadata.
  * debian/copyright: update years, and simplify the file.
  * Declare compliance with Debian Policy 4.5.1.
  * Set Rules-Requires-Root: no.
  * Annotate test-only build dependencies with <!nocheck>.
  * Bump debhelper-compat to 13.
  * Add debian/tests/pkg-perl/syntax-skip to run autopkgtest's syntax.t
    for all modules.

 -- gregor herrmann <gregoa@debian.org>  Sat, 21 Nov 2020 18:20:53 +0100

libconfig-model-systemd-perl (0.244.1-1) unstable; urgency=medium

  * New upstream version 0.244.1
    * update parameters from systemd 244 source
    * log at warn level when reading a sub layer file
    * don't mention migration in deprecation warnings
    * improve message when no info is found for a unit
    * use warn log to show user which resource is read
    * Fix to find service like Foo.service
  * control: depends on libconfig-model-tester-perl >= 4.005
  * use new debhelper dependency declararion
  * control: declare compliance with policy 4.4.1

 -- Dominique Dumont <dod@debian.org>  Sat, 28 Dec 2019 14:57:10 +0100

libconfig-model-systemd-perl (0.240.1-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.240.1
    * update parameters from systemd 240 source
    * use auto-delete to cleanup empty config files
      (which requires Config::Model 2.133)
  * Bump versioned (build) dependency on libconfig-model-perl.
  * Remove stray Homepage field in binary section.
  * Declare compliance with Debian Policy 4.3.0.
  * Bump debhelper compatibility level to 11.

 -- gregor herrmann <gregoa@debian.org>  Thu, 17 Jan 2019 19:51:14 +0100

libconfig-model-systemd-perl (0.239.1-1) unstable; urgency=medium

  [ Edward Betts ]
  * Correct spelling mistake in package description.

  [ Dominique Dumont ]
  * New upstream version 0.239.1
    * update from systemd 239 documentation
  * control: declare compliance with policy 4.1.5

 -- Dominique Dumont <dod@debian.org>  Tue, 17 Jul 2018 12:05:30 +0200

libconfig-model-systemd-perl (0.238.2-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Dominique Dumont ]
  * New upstream version 0.238.2
    * Fix file_path usage in Systemd* backends (Closes: #897963)
    * Show user message with User logger
    * added t/README.md
  * control:
    * depends on libconfig-model-perl >= 2.123
    * depends on libconfig-model-tester-perl >= 3.006
    * declare compliance with policy 4.1.4 (no other changes)

 -- Dominique Dumont <dod@debian.org>  Mon, 07 May 2018 18:35:36 +0200

libconfig-model-systemd-perl (0.238.1-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Dominique Dumont ]
  * New upstream version 0.238.1
    * Old systemd parameters are migrated to the new ones:
      * OnFailureIsolate in unit
      * RebootArgument
      * StartLimitInterval to StartLimitIntervalSec
      * SuccesAction and StartLimitBurst
      * FailureAction
    * updated from systemd 238 doc
    * Systemd comments are now preserved
  * control: depends on libconfig-model-perl >= 2.118
  * control: update Vcs-Browser and Vcs-Git

 -- Dominique Dumont <dod@debian.org>  Wed, 04 Apr 2018 14:05:22 +0200

libconfig-model-systemd-perl (0.236.1-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.2 (no changes needed)

  [ Dominique Dumont ]
  * New upstream version 0.236.1
    * add support for systemd 236
    * update upstream copyright year (cme update)

 -- Dominique Dumont <dod@debian.org>  Wed, 10 Jan 2018 19:01:03 +0100

libconfig-model-systemd-perl (0.235.1-1) unstable; urgency=medium

  * New upstream version 0.235.1
    * add support for systemd 235

 -- Dominique Dumont <dod@debian.org>  Mon, 16 Oct 2017 13:40:08 +0200

libconfig-model-systemd-perl (0.234.2-1) unstable; urgency=medium

  * New upstream version 0.234.2
    * all models use new rw_config parameter
  * control: depends on libconfig-model-perl >= 2.111
  * control: declare compliance with policy 4.1.1

 -- Dominique Dumont <dod@debian.org>  Tue, 10 Oct 2017 12:34:22 +0200

libconfig-model-systemd-perl (0.234.1-1) unstable; urgency=medium

  * New upstream version 0.234.1
    * add support for systemd 234
  * refreshed copyright with cme
  * control: declare compliance with policy 4.1.0

 -- Dominique Dumont <dod@debian.org>  Mon, 04 Sep 2017 20:07:32 +0200

libconfig-model-systemd-perl (0.232.7-1) unstable; urgency=medium

  * New upstream version 0.232.7:
    This release brings quite a big change to the way cme is invoked
    for systemd. "cme systemd" and "cme systemd-user" commands now expect
    an argument. Either a pattern to select service names to edit, or
    a unit name with or without service type. See Changes file and
    documentation for more details.
  * control: depends on libconfig-model-perl >= 2.104
  * control: declare compliance with policy 4.0.0
  * fix syntax issue of fill.copyright.blanks.yml

 -- Dominique Dumont <dod@debian.org>  Wed, 12 Jul 2017 19:25:45 +0200

libconfig-model-systemd-perl (0.232.6-1) unstable; urgency=medium

  * New upstream version 0.232.6
    * Respect paragraph format of original documentation. This
      improves a lot the readability of the documentation displayed in
      cme and on cpan website.

 -- Dominique Dumont <dod@debian.org>  Mon, 16 Jan 2017 11:43:37 +0100

libconfig-model-systemd-perl (0.232.5-1) unstable; urgency=medium

  * New upstream version 0.232.5
    * fix Unit Condition* parameters which are list type, not uniline
      (Closes: #849490)

 -- Dominique Dumont <dod@debian.org>  Sat, 14 Jan 2017 20:13:46 +0100

libconfig-model-systemd-perl (0.232.4-1) unstable; urgency=medium

  * New upstream version 0.232.4
    * fix systemd-user load when config dir is missing (Closes: #849490)

 -- Dominique Dumont <dod@debian.org>  Tue, 03 Jan 2017 21:21:00 +0100

libconfig-model-systemd-perl (0.232.3-1) unstable; urgency=medium

  * New upstream version 0.232.3
    * fix load of bad systemd files with -force option
    * issue an error when a systemd parameter is specified
      twice (can be overridden with -force option)
    * issue a warning when an unknown parameter is found in a
      systemd file
    * avoid writing systemd default values in systemd file
  * control: depends on libconfig-model-perl 2.096

 -- Dominique Dumont <dod@debian.org>  Tue, 13 Dec 2016 19:21:49 +0100

libconfig-model-systemd-perl (0.232.2-1) unstable; urgency=medium

  * New upstream version 0.232.2
    * update with systemd 232
    * can migrate deprecated resource-control parameters
      (for instance, cme replaces deprecated CPUWeight with CPUShares)
    * enable 2 styles of comments (gh #1)

 -- Dominique Dumont <dod@debian.org>  Sat, 26 Nov 2016 09:40:09 +0100

libconfig-model-systemd-perl (0.231.2-1) unstable; urgency=medium

  * New upstream version 0.231.2
    * Fix parser bug triggered by unit name containing a dot
  * control: Depends on libconfig-model-perl 2.094

 -- Dominique Dumont <dod@debian.org>  Sun, 13 Nov 2016 19:44:24 +0100

libconfig-model-systemd-perl (0.231.1-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Dominique Dumont ]
  * New upstream version 0.231.1
    * update with systemd 231
    * add support for Timer service
    * remove socket service file when needed

 -- Dominique Dumont <dod@debian.org>  Fri, 04 Nov 2016 13:01:46 +0100

libconfig-model-systemd-perl (0.007-1) unstable; urgency=medium

  * Imported Upstream version 0.007
    * Booleans are yes/no values
    * "boolean or something else" are now enum type with
      choices: yes,no,somethin-else as specified in Systemd doc

 -- Dominique Dumont <dod@debian.org>  Mon, 06 Jun 2016 19:22:04 +0200

libconfig-model-systemd-perl (0.006-1) unstable; urgency=medium

  * Imported Upstream version 0.006
  * control:
    * improved description (Closes: #823898)
      Tx Beatrice and Justin
    * recommends cme and libconfig-model-tkui-perl

 -- Dominique Dumont <dod@debian.org>  Thu, 12 May 2016 19:23:46 +0200

libconfig-model-systemd-perl (0.005-1) unstable; urgency=medium

  * Initial Release. (Closes: #822800)

 -- Dominique Dumont <dod@debian.org>  Mon, 09 May 2016 21:02:48 +0200
